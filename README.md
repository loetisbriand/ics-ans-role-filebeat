ics-ans-role-filebeat
===================

Ansible role to install filebeat.

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

Role Variables
--------------

```yaml
# -----------------------filebeat variables-----------------------

filebeat_gpg_key: https://packages.elastic.co/GPG-KEY-elasticsearch

filebeat_centos_mirror_url: https://artifacts.elastic.co

filebeat_conf_template:
  - name: config
    file: filebeat.yml.j2
    dest: /etc/filebeat/filebeat.yml

filebeat_repository:
  - baseurl: "{{ filebeat_centos_mirror_url }}/packages/{{ filebeat_version }}/yum"
    description: "Elastic repository for {{ filebeat_version }} packages"
    enabled: true
    gpgcheck: true
    gpgkey: "{{ filebeat_centos_mirror_url }}/GPG-KEY-elasticsearch"
    name: filebeat
    reposdir: /etc/yum.repos.d/

filebeat_version: 7.x

# -----------------------output variables-----------------------

filebeat_output: logstash  # if output is elasticsearch set elasticsearch

filebeat_output_logstash_hosts: localhost:5044
filebeat_output_elasticsearch_hosts: localhost:9200

filebeat_output_elasticsearch_protocol: https
filebeat_output_elasticsearch_username: elastic
filebeat_output_elasticsearch_password: change-me

filebeat_ssl_state: false
filebeat_ssl_cert: "/etc/filbeat/server.crt"
filebeat_ssl_key: "/etc/filbeat/server.key"
filebeat_ssl_auth: ["/etc/filebeat/ca.crt"]

filebeat_inputs_paths:
  - "{{ filebeat_zeek_log_path }}/*.log"
  - /var/log/*.log

# -----------------------modules variables---------------------------

filebeat_module: false

filebeat_template:
  - name: zeek
    file: zeek.yml.j2
    dest: /etc/filebeat/modules.d/zeek.yml

filebeat_zeek_log_path: /opt/zeek/logs/current

```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-filebeat
```

License
-------

BSD 2-clause

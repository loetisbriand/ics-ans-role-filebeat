import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('default_group')


def test_filebeat_running_and_enabled(host):
    filebeat = host.service("filebeat")
    assert filebeat.is_running
    assert filebeat.is_enabled
